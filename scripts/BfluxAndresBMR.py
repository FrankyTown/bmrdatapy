from fToWang import *

def BFluxAndres(autoHist = True):

	############### Analyse des BMRs d'une base de donné Andres ###############
	app = gui()
	AndyFil = getFile(app, ".dat file for Andres dataBase","data File","*.dat")
	#t(day) - CarLong - Lat - Angl sep - fluxes of pair - tilt angl
	NBMR,tabQte,cAndyTab = AndyReadFile(AndyFil)# fetches data from Andres database

	if autoHist:
		fluxHist(np.log10(abs(tabQte[:,4])),50)

	#Analyse Flux dist of Wang & Sheeley?!

	#print((cAndyTab.flux)[:])
	#print(cQ[1].toString())
	return cAndyTab,tabQte

#q,t = BFluxAndres()