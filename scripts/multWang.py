from fToWang import *


def mult_sign1():
    """qty multiplication for a Wang & Sheeley database format

    To improve: select exact index where you wanna start multiplying

    """

    while True:
        timesFlux = input("Multiply qty by: ")
        try:
            numTimesFlux = float(timesFlux)
        except ValueError:
            print("Must be an integer value!!! Try again...\n")
            continue
        break

    app = gui()
    WSFile = getFile(app, ".dat file for Wang dataBase","data File","*.dat")

    #t(day) - CarLong - Lat - Angl sep - fluxes of pair - tilt angl
    print("\nreading file...\n")
    header, duration, headTable, WStab, cWStab = WSReadFile(WSFile)

    path = app.directoryBox(title="Directory to write files", dirName=None)
    fullpath = os.path.join(path,"datMultBy_"+"{}"\
           .format(int(round(numTimesFlux*100)))+"pc.dat")
    print("writing file...\n")
    with open(fullpath,"w") as file:
        #print(header)
        file.write(header)
        file.write(duration)
        file.write(headTable)
        for i in range(len(cWStab)):
            cWStab[i].lat1 = 180 - cWStab[i].lat1
            cWStab[i].lat2 = 180 - cWStab[i].lat2
            cWStab[i].day += 14688.0
            file.write(cWStab[i].toString())

mult_sign1()
