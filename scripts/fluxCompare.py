from BfluxAndresBMR import *
from BfluxWangBMR import *

def gauss(x,mu,sigma):
    return np.exp(-(x-mu)**2/2/sigma**2)/np.sqrt(2*np.pi)/sigma

def main():
    # Wang & Sheeley
    cWS, tabWS = BFluxWS(False)
    plt.hist(np.log10(tabWS[:,5]*1e21),10, density=True,
            label = "Wang & Sheeley BMRs")

    # Andres
    cAndy,tabAndy = BFluxAndres(False)
    plt.hist(np.log10(abs(tabAndy[:,4])),50, density=True, alpha=0.8,
             label = "ARY BMRs")

    #Gauss ---------
    x = np.linspace(19,24,100)

    meanFluxWang = np.mean(np.log10(tabWS[:,5]*1e21))
    stdFluxWang  = np.std(np.log10(tabWS[:,5]*1e21))
    gWang = gauss(x,meanFluxWang,stdFluxWang)
    plt.plot(x,gWang,color = "blue",label = "mean flux: {:.2E}"\
            .format(np.mean(tabWS[:,5]*1e21)))

    meanFluxAndy = np.mean(np.log10(abs(tabAndy[:,4])))
    stdFluxAndy  = np.std(np.log10(abs(tabAndy[:,4])))
    gAndy = gauss(x,meanFluxAndy,stdFluxAndy)
    plt.plot(x,gAndy,color = "orange",\
            label = "mean flux: {:.2E}".format(np.mean(abs(tabAndy[:,4]))))
    # --------------

    # Flux total
    totFluxWang = sum(tabWS[:,5]*1e21)
    totFluxAndy = sum(abs(tabAndy[:,4]))

    # Ploting Time specs
    #plt.title("Total BFlux W&S: {:.2E}, ".format(totFluxWang)\
    #    +"Total BFlux ARY: {:.2E}".format(totFluxAndy))
    plt.xlabel("Log($\phi$)", fontsize=15)
    plt.ylabel("$f_{ Log(\phi)}$", fontsize=15)
    plt.legend()
    plt.show()

main()
