import numpy as np
import matplotlib.pyplot as plt
import sys
from scipy.stats.stats import pearsonr
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit

def linFuncBy0(a,x):
    return a*x

def polyFunc(x, a, b, c, d):
    return a * x**3 + b * x**2 + c * x + d

def expoFunc(x, a, b, c):
    return a * np.exp(b * x) + c

def mcarre(x, y):
    """Least square function for computing m and b in linear regression
    """
    N = len(x)
    X, Y, XY, XX = sum(x)/N, sum(y)/N, sum(x*y)/N, sum(x**2)/N

    m = (XY-X*Y)/(XX-X*X)
    b = Y-m*X
    return m, b

runFile = "AndresRun/median_flux.txt" #sys.argv[1]
SIDCFile = "SIDC_data/SN_m_cyc23_24.txt" #sys.argv[2]
#correlate(runFile, SIDCfile

print("reading endFile from model run of Munoz-Jaramillo databasse...")
with open(runFile, 'r') as file:
    runTab = []
    for line in file:
        runTab.append(line.split())
runTab = np.array(runTab).astype(float)
modTime, modPseudoSSN = runTab[:,0], runTab[:,1]

print("reading SIDC SSN database...")
with open(SIDCFile, 'r') as file:
    SIDCTab = []
    for line in file:
        SIDCTab.append(line.split())

SIDCTab = np.array(SIDCTab).astype(float)
sTime, sSSN, sSigma = SIDCTab[:,2], SIDCTab[:,3], SIDCTab[:,4]

# interpolate
func = interp1d(sTime, sSSN)
projSSNInterp = func(modTime)

# linear fit
r = pearsonr(modPseudoSSN, projSSNInterp)
print("\n  |Pearson coeff: {}".format(r[0]))
m, b = mcarre(modPseudoSSN, projSSNInterp)
print("  |linear fit: y={:.2f}x+{:.2f}".format(m, b))

# lin fit through 0
linp, linpc = curve_fit(linFuncBy0, modPseudoSSN, projSSNInterp)
perr = np.sqrt(np.diag(linpc))
print("  |lin fit by 0 outs:", linp, '+-',perr)

# polynomial fit
popt, pcov = curve_fit(polyFunc, modPseudoSSN, projSSNInterp)
print("  |polynomial fit ouputs:", popt)

# expo fit
#eopt, ecov = curve_fit(expoFunc, modPseudoSSN, projSSNInterp)
#print("exponential fit outputs:", eopt)

# plotting time --------------------
f = plt.figure(0)
ax1 = plt.subplot(211)
ax1.plot(modTime, modPseudoSSN, '.-', label=r'$\Phi(t)$')
ax1.plot(modTime[0:98], projSSNInterp[0:98], '.-',\
         label='interpolation cycle 23', color='red')
ax1.plot(modTime[98:], projSSNInterp[98:], '.-',\
         label='interpolation cycle 24', color='black')
ax1.plot(sTime, sSSN, '.-', label="SIDC SSN") #add errobars
ax1.legend(fontsize=13)
ax1.set_xlabel("temps [années]", fontsize=15)
#ax1.set_ylabel("", fontsize=15)
ax2 = plt.subplot(212)
ax2.plot(modPseudoSSN[0:98], projSSNInterp[0:98], '.', color='red')
ax2.plot(modPseudoSSN[98:], projSSNInterp[98:], '.', color='black')
synthSSN = np.linspace(0, max(modPseudoSSN), 100)
ax2.plot(synthSSN, m*synthSSN + b, label="lin")
#        "lin: y={:.2f}x+{:.2f} with P. coeff={:.2f}".format(m, b, r[0]))
ax2.plot(synthSSN, polyFunc(synthSSN, *popt), '-', label ="polynomial")
#        "poly: y={:.2f}x³+{:.2f}x²+{:.2f}x+{:.2f}"\
#        .format(*popt))
#ax2.plot(synthSSN, expoFunc(synthSSN, *eopt), '-', label = \
#        "expo: y = {:.2f}exp({:.2f}x)+{:.2f}".format(*eopt))
ax2.plot(synthSSN, linFuncBy0(synthSSN, *linp), '-', label = r"lin$_0$")
#        "lin0: y = ({:.2f}+-{:.2f})x".format(*linp, *perr))
ax2.legend(fontsize=13)
ax2.set_xlabel(r"$\Phi(t)_{mod}$ [Mx]", fontsize=15)
ax2.set_ylabel(r"$SSN_{SIDC}$", fontsize=15)
plt.show()


