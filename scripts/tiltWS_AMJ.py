import numpy as np
import matplotlib.pyplot as plt
import sys
from fToWang import AndyReadFile, WSReadFile

def gauss(x,mu,sigma):
    return np.exp(-(x-mu)**2/2/sigma**2)/np.sqrt(2*np.pi)/sigma

def WStiltFinder(lats1, longs1, lats2, longs2):
    """Converts WS longitude and latitude coordinates to tilt measurements
    for spherical triangle:   .B
                            a /  \ c
                              /    \                                         .
                             C.T____.A
                                 b
    with right angle C: a = latB-latC, b = longC-longB
    A is colat2 and long2, B is colat1 and long1
    """
    # length of sides computing time
    a = np.deg2rad(lats1-lats2)
    b = np.deg2rad(longs2-longs1)

    return np.arctan(np.tan(a)/np.sin(b))

def WS_SepFinder(lats1, longs1, lats2, longs2):
    a = np.deg2rad(lats1-lats2)
    b = np.deg2rad(longs2-longs1)
    return np.arccos(np.cos(a)*np.cos(b))

APath = "AndresEnd2017/2018_06_21_bmrs.dat" #sys.argv[1]
WSPath = "WangRun/dat/full.dat"#sys.argv[2]
#WSPath = "../0-data/2-wang/wang_alex.dat"

""" 1st arg: ARY database path
2nd arg: WS database path
"""
# read WS and ARY data
print()
print("reading ARY file")
NBMR, ATab, cATab = AndyReadFile(APath)
ATilts = ATab[:,-1]
print("reading WS file")
header, dur, hTable, WSTab, cWSTab = WSReadFile(WSPath)

# center latitude of bipole
print("computing center latitude of bipoles")
centA_BMR = ATab[:,2]
centWS_BMR = (WSTab[:,7]+WSTab[:,9])/2

# compute tilts in WS data
print("computing tilts")
WSTilts = WStiltFinder(WSTab[:,7], WSTab[:,8], WSTab[:,9], WSTab[:,10])

# convert tilts from radiants to degrees
dWSTilts = np.rad2deg(WSTilts)

# convert to absolute tilt angle
ab ,= np.where(centA_BMR<=0)
aa ,= np.where(centA_BMR>=0)
ATilts[ab]*=-1
ab ,= np.where(centWS_BMR-90<=0)
dWSTilts[ab]*=-1

# means and stds of titls
A_Mean, WS_Mean = np.mean(ATilts), np.mean(dWSTilts)
A_std, WS_std = np.std(ATilts), np.std(dWSTilts)
print("ARY titls: ", A_Mean, '+-', A_std)
print("WS tilts: ", WS_Mean, '+-', WS_std)

# plotting time ---------------------------------------------------------------
print("ploting tilts time")
f = plt.figure(0)
ax0 = plt.subplot(2,1,1)
ax0.plot(abs(centA_BMR), ATilts, '.',
         label=r'ARY $\left<\alpha\right>\pm\sigma = {:.2f}\pm{:.2f}[\degree]$'
         .format(A_Mean, A_std), color='darkorange')
ax0.plot(abs(centWS_BMR-90), dWSTilts, '.',
         label=r'WS $\left<\alpha\right>\pm\sigma = {:.2f}\pm{:.2f}[\degree]$'
         .format(WS_Mean, WS_std), color='royalblue')
#ax0.set_xlim(-50,50)
ax0.set_ylabel(r"inclinaisons [$\degree$]", fontsize=15)
ax0.set_xlabel(r"latitude [$\degree$]", fontsize=15)
ax0.legend(fontsize=10)

# histograms
ax1 = plt.subplot(2,2,3)
ax1.hist(ATilts, bins = 40, density = True, label = 'ARY',
         color='darkorange')
#ax1.hist(ATab[ab,2][0], bins = 40, density = True, label = 'ARY',
#         color='orange')
ax1.set_xlabel(r"inclinaisons [$\degree$]", fontsize=15)

ax2 = plt.subplot(2,2,4)
ax2.hist(dWSTilts, bins = 40, density = True,\
         label = 'WS')
ax2.set_xlabel(r"inclinaisons [$\degree$]", fontsize=15)

# find angular sep
print("computing angular separations devided by flux")
A_Sep = ATab[:,3]#/abs(ATab[:,4])
WS_Sep = WS_SepFinder(WSTab[:,7], WSTab[:,8], WSTab[:,9],
                      WSTab[:,10])#/(WSTab[:,5])

# convert WS angular sep from radiants to degrees
dWS_Sep = np.rad2deg(WS_Sep)

# means and std of angular sep
Asep_mean, WSsep_mean = np.mean(A_Sep), np.mean(dWS_Sep)
Asep_std, WSsep_std = np.std(A_Sep), np.std(dWS_Sep)
print("ARY angular separations: ",np.mean(A_Sep), '+-', Asep_std)
print("WS angular separations: ", np.mean(dWS_Sep), '+-', WSsep_std)

# plotting time ---------------------------------------------------------------
print("plotting angular seprations")
fAng = plt.figure(1)
axs0 = plt.subplot(2,1,1)
axs0.loglog(WSTab[:,5]*10**21, dWS_Sep, '.', label = "WS",
            color='royalblue') #centWS_BMR-90
axs0.loglog(abs(ATab[:,4]), A_Sep, '.', label = "ARY",
            color='darkorange')#centA_BMR
axs0.set_ylabel(r"$\delta$ [$\degree$]", fontsize=15)
axs0.set_xlabel(r"$\Phi [Mx]$", fontsize=15)
axs0.legend(fontsize=10)

# histograms
axs1 = plt.subplot(2,2,3)
axs1.hist(A_Sep, bins=40, cumulative=True, density=True, histtype='step',
          label='ARY', color='darkorange')
axs1.axvline(x=4.79, color='darkorange')
axs1.set_xlabel(r"$\delta$ [$\degree$]", fontsize=15)

axs2 = plt.subplot(2,2,4)
axs2.hist(dWS_Sep, bins=20, density=True, cumulative=True, histtype='step',\
        label='WS', range=(0,15), color='royalblue')
axs2.axvline(x=9.78, color='royalblue')

axs2.set_xlabel(r"$\delta$ [$\degree$]", fontsize=15)
plt.show()
