from fToWang import *

def BFluxWS(autoHist = True):
	app = gui()
	# getting file
	WSFile = getFile(app, ".dat file for Wang & Sheeley dataBase","data File","*.dat")

	# read file
	header, duration, headcolumn, WStab, cWStab = WSReadFile(WSFile)

	if autoHist:
		fluxHist(np.log10(WStab[:,5]*1e21),20)

	return cWStab, WStab

#c,t = BFluxWS()
