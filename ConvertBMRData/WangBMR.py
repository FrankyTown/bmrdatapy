"""WangBMR class def
"""


class WangBMR:
    """a BMR is a pyhton pbject with propreties:
    MBR# cycle sign dateUT day flux sign1 lat1 long1 lat2 long2
    """
    def __init__(self, MBRn, day, longW, longE, latW, latE, flux, sign1):
        self.MBRn = MBRn      # number of BMR in list
        self.cycle = 1        # always 1
        self.sign = 0         # always 0
        self.dateUT = 999901010003  # always that
        self.day = day        # day of emergene
        self.flux = flux
        self.sign1 = sign1    # sign of first polarity
        self.lat1 = latE      # latitude of Eastern polarity
        self.lat2 = latW      # ... of western polarity
        self.long1 = longE    # longitude of Eastern polarity
        self.long2 = longW    # .. of Western polarity

    def toString(self):       # string of qties with right format
        return "   {:>4} {:>5d} {:>4d} {:>14d} {:>12.4f} {:>13.2f} {:>5d} {:>6.1f} {:>6.1f} {:>6.1f} {:>6.1f}\n"\
                            .format(int(self.MBRn),int(self.cycle),int(self.sign),\
                            int(self.dateUT),self.day,self.flux,int(self.sign1),\
                            self.lat2,self.long2,self.lat1,self.long1)
