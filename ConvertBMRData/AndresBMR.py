"""AndresBMR class definition
"""
import numpy as np

class AndresBMR():
    """Day of central-meridian crossing (numbered sequentially with 1 being 1st
       January 1996).
     Carrington longitude of bipole centre (degrees).
     Latitude of bipole centre (degrees).
     Angular separation between the centroids of each polarity, in heliocentric
            degrees.
     Total magnetic flux of each polarity (Maxwells). The sign indicates the
            polarity: + means the negative polarity is leading (westward) while -
            means the positive polarity is leading.
    Tilt angle, i.e. the angle between a line connecting the centroids and the
            east-west direction.degrees
    """
    def __init__(self, nBMR, day, carLong, lat, angSep, sign, flux, tilt):
        self.nBMR       = nBMR
        self.day        = day
        self.carLong= carLong
        self.lat        = lat
        self.angSep = angSep
        self.sign       = sign
        self.flux       = flux
        self.tilt       = tilt

    def toString(self):
        return "        "+"{:>4}".format(self.day.astype(int))\
         +"{:>13.3f}".format(self.carLong)\
         +"{:>13.4f}".format(self.lat)\
         +"{:>13.5f}".format(self.angSep)\
         +"{:>13.5E}".format(self.flux)\
         +"{:>13.4f}\n".format(self.tilt)

