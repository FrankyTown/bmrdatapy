import numpy as np
import matplotlib.pyplot as plt


def longOfPair(longi, angSep, tilt):
    angSep = np.deg2rad(angSep)
    tilt = np.deg2rad(tilt)

    # old : angSep/2
    longW = longi - np.rad2deg(np.arctan(np.cos(tilt)*np.tan(angSep/2)))
    longE = longi + np.rad2deg(np.arctan(np.cos(tilt)*np.tan(angSep/2)))
    #print(longW,longE)
    return longW,longE

def latOfPair(lat,angSep,tilt):
    angSep = np.deg2rad(angSep)
    tilt = np.deg2rad(tilt)

    # old :angSep/2*np.tan(tilt)+90
    latW = lat - np.rad2deg(np.arcsin(np.sin(tilt)*np.sin(angSep/2)))+90
    latE = lat + np.rad2deg(np.arcsin(np.sin(tilt)*np.sin(angSep/2)))+90
    #print(latW,latE)
    return latW,latE

def moyFlux():
    return "hello"

def getFile(app,title,fileType,showFiles):
    return app.openBox(title=title,\
     dirName=None, fileTypes=[(fileType,showFiles)], asFile=False)

def AndyReadFile(fileToRead):
    file = open(fileToRead,"r")
    nBMR = int(file.readline())
    # initiate array with axis not for axis = 0
    qties = np.array([file.readline().split(),file.readline().split()])
    # Fill vector qties
    for line in file:
            qties = np.append(qties,np.array([line.split()]),axis = 0)
    file.close()

    qties = qties.astype(float)

    cQties = np.array([])
    n=0
    for b in qties:
            cQties = np.append(cQties,AndresBMR(n,b[0],b[1],b[2],b[3],\
                    Sign1(b[4]),b[4],b[5]))
            n+=1
    return nBMR,qties,cQties

def WSReadFile(fileToRead):
    file  = open(fileToRead,"r")
    # header and column heads
    header = file.readline()#+"\n"
    duration = file.readline()#+"\n"
    headTable = file.readline()#+"\n"

    WStab = np.array([file.readline().split(),file.readline().split()])
    for line in file:
        WStab = np.append(WStab,np.array([line.split()]),axis = 0)
    file.close()

    WStab = WStab.astype(float)
    #print(WStab[0])
    cWStab = np.array([])
    for b in WStab:
        # MBR# cycle sign dateUT day flux sign1 lat1 long1 lat2 long2
        cWStab = np.append(cWStab, WangBMR(b[0],b[4],b[8],b[10],\
                b[7],b[9],b[5],b[6]))

    return header, duration, headTable, WStab, cWStab


def Sign1(flux):
    return (flux>0).astype(int)-(flux<0).astype(int)


############################## ploting time ###################################
def fluxHist(flux,nBins):
    plt.hist(flux,nBins,normed=False)
    plt.show()
    return "hello"
