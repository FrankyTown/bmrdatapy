from fToWang import *
import WangBMR
import AndresBMR

def ToWangSheeley():
    """Convert Andrez Munoz bipolar magnetic region (BMR) data to a Wang &
       Sheeley data format to input in 2times2x2BLModel dynamo model of surface
       transport of magnetic field on the solar surface.

       Write output file in W&S format to be read by the input mode of the
       model.
    """
    while True:
        timesFlux = input("Multiply flux by: ")
        try:
                numTimesFlux = float(timesFlux)
        except ValueError:
                print("Must be an integer value!!! Try again...\n")
                continue
        break

    app = gui()
    AndyFil = getFile(app, ".dat file for Andres dataBase","data File","*.dat")

    print("\nreading file...\n")

    # fetch data from Andres database with format:
    #t(day) - CarLong - Lat - Angl sep - fluxes of pair - tilt angl
    NBMR,tabQte,tataC = AndyReadFile(AndyFil)

    # Computing different WangBMRs qties from Andres Munoz database Sunspots
    # and filling wangList
    print("converting stuff...\n")
    duration =tabQte[-1,0]-tabQte[0,0]              # Duration of database
    wangList = []

    for i in range(0,NBMR):
        latW,latE = latOfPair(tabQte[i,2],tabQte[i,3],tabQte[i,5])
        longW,longE = longOfPair(tabQte[i,1],tabQte[i,3],tabQte[i,5])
        day = tabQte[i,0]-177                   # for start on day=181
        flux = tabQte[i,4]                      # abs flux because of Sign1 def
        sign1 = Sign1(flux)
        wangList.append(WangBMR(i+1, day, longW, longE, latW, latE,
                                numTimesFlux*abs(flux/10**21), sign1))

    # Wang and Shealey database type header
    header  = "BLDYN self-generated data      Duration (days)\n"\
                    +"{:>45.4f}".format(duration)\
                    +"\n   MBR# Cycle Sign      Date & UT"\
                    +"   Time(days) Flux(10^21Mx)"\
                    +" Sign1 Colat1  Long1 Colat2  Long2\n"

    # Writing Andres.dat in chosen directory
    path = app.directoryBox(title="Directory to write files", dirName=None,)
    fullpath = os.path.join(path,"Andres"+"{}".format(int(round(numTimesFlux*100)))+"pc.dat")
    print("writing file...\n")
    with open(fullpath,"w") as file:
        file.write(header)
        for b in wangList:
            file.write(b.toString())

    return wangList

wangList = ToWangSheeley()
